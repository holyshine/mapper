import asyncio
import multiprocessing
import time
from urllib.parse import urljoin, urlparse

import aiohttp
import urllib3

from bs4 import BeautifulSoup
from queue import Queue


class AsyncMapper:
    basestring = ''

    def __init__(self, url):
        self.http = urllib3.PoolManager()
        self.base_url = url
        self.scraped_pages = set()
        self.root_url = '{}://{}'.format(urlparse(self.base_url).scheme, urlparse(self.base_url).netloc)
        self.__tree = ''
        self.to_queue = asyncio.Queue()
        self.to_queue.put(self.base_url)

    async def parse_links(self, url=None):
        if url is None:
            url = self.to_queue.get()

        res = self.scrape_page(url)
        soup = BeautifulSoup(res.data, 'html.parser')
        links = set(soup.find_all('a', href=True))
        for link in links:
            selected_url = link['href']
            if selected_url.startswith('/') or selected_url.startswith(self.root_url):
                join_url = urljoin(self.root_url, selected_url)
                if join_url not in self.scraped_pages:
                    self.scraped_pages.add(join_url)
                    self.__tree += '{}\n'.format(join_url)
                    await self.to_queue.put(join_url)

    def scrape_page(self, url):
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)'
        http = urllib3.PoolManager()
        response = http.request('GET', url, headers={'User-Agent': user_agent})
        return response

    def run_scraper(self):
        self.parse_links(self.base_url)
        loop = asyncio.get_event_loop()
        if not self.to_queue.empty():
            final_tasks = asyncio.gather([self.parse_links(self.to_queue.get()) for _ in range(self.to_queue.qsize())])
            loop.run_until_complete(final_tasks)

    def show_tree(self):
        return self.__tree


if __name__ == '__main__':
    start_time = time.time()
    a = AsyncMapper('https://translate.google.com')
    a.run_scraper()
    print(a.show_tree())
    print(time.time() - start_time)
