import multiprocessing
from urllib.parse import urljoin, urlparse

import urllib3

from bs4 import BeautifulSoup
from queue import Queue
from abc import ABCMeta, abstractmethod


class MultiMapper(metaclass=ABCMeta):
    basestring = ''

    def __init__(self, url):
        self.http = urllib3.PoolManager()
        self.indents = []
        self.base_url = url
        self.scraped_pages = set()
        self.root_url = '{}://{}'.format(urlparse(self.base_url).scheme, urlparse(self.base_url).netloc)
        self.__tree = ''
        self.to_queue = Queue()
        self.to_queue.put(self.base_url)

    def parse_links(self, url=None, indent=0):
        if url is None:
            url = self.to_queue.get()

        res = self.scrape_page(url)
        soup = BeautifulSoup(res.data, 'html.parser')
        links = set(soup.find_all('a', href=True))
        for link in links:
            selected_url = link['href']
            if selected_url.startswith('/') or selected_url.startswith(self.root_url):
                join_url = urljoin(self.root_url, selected_url)
                if join_url not in self.scraped_pages:
                    self.scraped_pages.add(join_url)
                    self.__tree += '{}\n'.format(join_url)
                    self.to_queue.put(join_url)

    def scrape_page(self, url):
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)'
        http = urllib3.PoolManager()
        response = http.request('GET', url, headers={'User-Agent': user_agent})
        return response

    @abstractmethod
    def start_action(self):
        pass

    def run_scraper(self):
        self.parse_links(self.base_url)
        self.indents = [0 for _ in range(multiprocessing.cpu_count() * 2)]
        while not self.to_queue.empty():
            for _ in range(multiprocessing.cpu_count() * 2):
                t = self.start_action()
                t.start()

    def show_tree(self):
        return self.__tree
