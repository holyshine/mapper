import multiprocessing
import time

from parser_app.multi_mapper import MultiMapper


class ProcessMapper(MultiMapper):
    def start_action(self):
        return multiprocessing.Process(target=self.parse_links())


if __name__ == '__main__':
    start_time = time.time()
    a = ProcessMapper('https://translate.google.com')
    a.run_scraper()
    print(a.show_tree())
    print(time.time() - start_time)
