import time
import threading

from parser_app.multi_mapper import MultiMapper


class ThreadMapper(MultiMapper):
    def start_action(self):
        url = self.to_queue.get()
        return threading.Thread(target=self.parse_links, args=(url,))


if __name__ == '__main__':
    start_time = time.time()
    a = ThreadMapper('https://translate.google.com')
    a.run_scraper()
    print(a.show_tree())
    print(time.time() - start_time)
