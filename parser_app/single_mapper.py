from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse
from urllib3 import PoolManager

import time


class SingleMapper:
    __tree = ''

    def __init__(self, url):
        self.base_url = url
        self.scraped_pages = set()
        self.root_url = '{}://{}'.format(urlparse(self.base_url).scheme, urlparse(self.base_url).netloc)

    def parse_links(self, html, indent=0):
        soup = BeautifulSoup(html, 'html.parser')
        links = set(soup.find_all('a', href=True))
        for link in links:
            url = link['href']
            if url.startswith('/') or url.startswith(self.root_url):
                url = urljoin(self.root_url, url)
                if url not in self.scraped_pages:
                    self.scraped_pages.add(url)
                    print('{}{}'.format('\t' * indent, url))
                    self.__tree += '{}{}\n'.format('\t' * indent, url)
                    res = self.scrape_page(url)
                    self.parse_links(res.data, indent + 1)

    def scrape_page(self, url):
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3)'
        http = PoolManager()
        response = http.request('GET', url, headers={'User-Agent': user_agent})
        return response

    def run_scraper(self):
        self.scraped_pages.add(self.base_url)
        res = self.scrape_page(self.base_url)
        self.parse_links(res.data)

    def show_tree(self):
        return self.__tree


if __name__ == '__main__':
    start_time = time.time()
    a = SingleMapper('https://translate.google.com')
    a.run_scraper()
    print(a.show_tree())
    print(time.time() - start_time)
