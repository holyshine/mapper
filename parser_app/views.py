import json

from django.http import HttpResponse
from django.views import View
from parser_app.single_mapper import SingleMapper


class ParseView(View):
    def post(self, request):
        data = json.loads(request.body)
        instance = Mapper(data.get('url'))
        return HttpResponse(instance.show_tree(), status=200)


class ThreadParseView(View):
    def post(self, request):
        data = json.loads(request.body)
        instance = Mapper(data.get('url'))
        return HttpResponse(instance.show_tree(), status=200)
